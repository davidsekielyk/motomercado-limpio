let orderFormInnew;

$(document).ready(()=> {
	getOrderFormInnew()
	.then( orderForm => {
		orderFormInnew = orderForm
		setQtCart()
	})
})

function getOrderFormInnew() {
	return new Promise( (res, rej) => {
		vtexjs.checkout.getOrderForm()
		.then( orderForm => {
			res(orderForm)
		})
	})
}

function setQtCart(){

	let cantSkus = 0
	orderFormInnew.items.forEach(item => {
		cantSkus += item.quantity
	});

	console.log('cantidad total:',cantSkus)
	$(".shopcar__cant").html(cantSkus)

}

(()=>{

	navScroll()
	buscador()
	buscadorFocus()
	getListaBuscador()
	navMenu()
	subMenu()
	menuTercernivel()
	bodyNoScroll()

})()

// navbar se oculta al bajar - se ve al subir
function navScroll(){
	const body = document.body;
	const scrollUp = "scroll-up";
	const scrollDown = "scroll-down";
	let lastScroll = 0;

	window.addEventListener("scroll", () => {
		const currentScroll = window.pageYOffset;
		if (currentScroll <= 90) {
			body.classList.remove(scrollUp);
			return;
		}
		
		if (currentScroll > lastScroll && !body.classList.contains(scrollDown)) {
			// down
			body.classList.remove(scrollUp);
			body.classList.add(scrollDown);
		} else if (currentScroll < lastScroll && body.classList.contains(scrollDown)) {
			// up
			body.classList.remove(scrollDown);
			body.classList.add(scrollUp);
		}
		lastScroll = currentScroll;
	})
}


function buscador(){

	$("body").on("click",".header-navbar__icons-item-search,.header-top__bg-diag-search", function(){
		$(".header-navbar__header-search,.ui-autocomplete.ui-menu.ui-widget.ui-widget-content.ui-corner-all,.header-navbar__icons-item-search, .bg__shadow-search").addClass("show")
	})

	$("body").on("click",".header-navbar__icons-item-search.show,.bg__shadow-search.show,.header__search-close", function(){
		$(".header-navbar__header-search,.ui-autocomplete.ui-menu.ui-widget.ui-widget-content.ui-corner-all,.header-navbar__icons-item-search, .bg__shadow-search").removeClass("show")
	}) 
	
	$("body").on("click",".header-navbar__icons-item-search,.header-top__bg-diag-search", function(){
		$("header").addClass("block")
	})

	$("body").on("click",".header-navbar__icons-item-search.show,.bg__shadow-search.show,.header__search-close", function(){
		$("header.block").removeClass("block")
	})
}

function buscadorFocus(){

	$(".header-navbar__icons-item-search,.header-top__bg-diag-search").click(function() {
		$(".fulltext-search-box").focus();
	})

}

function getListaBuscador() {
	const target = document.querySelector('.ui-autocomplete.ui-menu.ui-widget ')
	// create an observer instance
	const observer = new MutationObserver(function (mutations) {
		editListaBuscador()
	});
	// configuration of the observer:
	const config = { attributes: false, childList: true, characterData: false };
	// pass in the target node, as well as the observer options
	observer.observe(target, config);
}

function editListaBuscador(){
	// console.log("termino de cargar la lista de opciones")
	const listProduct = $(".ui-autocomplete.ui-menu.ui-widget li")
	for(let i = 0; i < listProduct.length; i++){
			//console.log(listProduct[i])
			const item = listProduct.eq(i)
			// console.log(item)
			if(!item.find("img").length){
					// console.log("no tiene imagen")
					item.addClass("hidden")
			}else{
					// console.log("si tiene imagen")
					const src = item.find("img").attr("src")
					// console.log("src viejo",src)
					const nuevoSrc = src.replace("-25-25","-100-100")
					// console.log("src nuevo", nuevoSrc)
					item.find("img").attr("src",nuevoSrc)
			}
	}
}

function navMenu(){

	$("body").on("click",".header-navbar__menu-icon", function(){
		$(".header-navbar__menu,.header-navbar__menu-icon,.bg__shadow,.button__closed").addClass("show")
	})

	$("body").on("click",".header-navbar__menu-icon.show,.bg__shadow.show,.button__closed.show", function(){
		$(".header-navbar__menu,.header-navbar__menu-icon,.bg__shadow,.header-navbar__submenu.one,.button__closed").removeClass("show")
	}) 
}

function subMenu(){
	$("body").on("click",".header-navbar__item.one", function(){
		$(".header-navbar__submenu.one,.header-navbar__submenu-cat-one").addClass("show")
	})

	$("body").on("click",".header-navbar__submenu-cat-one,.header-navbar__menu-icon.show,.bg__shadow.show", function(){
		$(".header-navbar__submenu.one,.header-navbar__submenu-cat-one").removeClass("show")
	})

	$("body").on("click",".header-navbar__item.two", function(){
		$(".header-navbar__submenu.two,.header-navbar__submenu-cat-two").addClass("show")
	})

	$("body").on("click",".header-navbar__submenu-cat-two,.header-navbar__menu-icon.show,.bg__shadow.show", function(){
		$(".header-navbar__submenu.two,.header-navbar__submenu-cat-two").removeClass("show")
	})

	$("body").on("click",".header-navbar__item.three", function(){
		$(".header-navbar__submenu.three,.header-navbar__submenu-cat-three").addClass("show")
	})

	$("body").on("click",".header-navbar__submenu-cat-three,.header-navbar__menu-icon.show,.bg__shadow.show", function(){
		$(".header-navbar__submenu.three,.header-navbar__submenu-cat-three").removeClass("show")
	})

	$("body").on("click",".header-navbar__item.four", function(){
		$(".header-navbar__submenu.four,.header-navbar__submenu-cat-four").addClass("show")
	})

	$("body").on("click",".header-navbar__submenu-cat-four,.header-navbar__menu-icon.show,.bg__shadow.show", function(){
		$(".header-navbar__submenu.four,.header-navbar__submenu-cat-four").removeClass("show")
	})

	$("body").on("click",".header-navbar__item.five", function(){
		$(".header-navbar__submenu.five,.header-navbar__submenu-cat-five").addClass("show")
	})

	$("body").on("click",".header-navbar__submenu-cat-five,.header-navbar__menu-icon.show,.bg__shadow.show", function(){
		$(".header-navbar__submenu.five,.header-navbar__submenu-cat-five").removeClass("show")
	})

	$("body").on("click",".header-navbar__item.six", function(){
		$(".header-navbar__submenu.six,.header-navbar__submenu-cat-six").addClass("show")
	})

	$("body").on("click",".header-navbar__submenu-cat-six,.header-navbar__menu-icon.show,.bg__shadow.show", function(){
		$(".header-navbar__submenu.six,.header-navbar__submenu-cat-six").removeClass("show")
	})
}

function menuTercernivel(){
	$('ul.header-navbar__menutercernivel').hide();

		$('.header-navbar-submenu__item').click(function(event) {
				event.stopPropagation();
				$('> ul', this).toggle();

		});
}

function bodyNoScroll(){
	
	$("body").on("click",".header-navbar__menu-icon,.header-navbar__icons-item-search,.header-top__bg-diag-search", function(){
		$("body").addClass("no_scroll");
	})

	$("body").on("click",".header-navbar__menu-icon.show,.bg__shadow-search.show,.bg__shadow.show,.header__search-close,.button__closed.show", function(){
		$("body").removeClass("no_scroll");
	})

}

function verMas(){
	$("header").on("click",".header-navbar-submenu__vermas", function(){
		$(this).next("ul.header_navbar-submenu__oculto").addClass("show");
	})
}

/* function menus(){

	if($(window).width() <= 1024){

		$(document).ready(function(){
				$('.header-navbar__item a').on("click", function(e){
					$(this).next('ul').addClass("show")
					e.stopPropagation();
					e.preventDefault();
				});
			});

}} */


function getDataNav(){
	$.ajax({
		url: "/api/catalog_system/pub/facets/search/respuesto?map=c",
		success: function(data){
			console.log(data.CategoriesTrees)
			createNav(data.CategoriesTrees)
		}
	})
}
getDataNav()

function createNav(CategoriesTrees){
	CategoriesTrees.forEach(depto => {
		console.log('depto',depto)

		depto.Children.forEach(categoria => {
			console.log('categoria',categoria)

			$(`.header-navbar__container-col-4.id-${depto.id}`).append(`
				<li class="header-navbar-submenu__item">
					<a class="header-navbar-submenu__item-tittle" href="#">
							${categoria.Name}
							<svg width="6" height="10" viewBox="0 0 6 10" fill="none" xmlns="http://www.w3.org/2000/svg">
									<path
											d="M0.540537 0.159839L0.210697 0.487403C0.107688 0.590493 0.0509398 0.727648 0.0509398 0.874234C0.0509398 1.02074 0.107688 1.15806 0.210697 1.26115L3.94747 4.99776L0.206551 8.73869C0.103542 8.84162 0.046875 8.97893 0.046875 9.12544C0.046875 9.27195 0.103542 9.40934 0.206551 9.51235L0.534359 9.84C0.747531 10.0533 1.09477 10.0533 1.30794 9.84L5.77798 5.38598C5.8809 5.28305 5.95343 5.1459 5.95343 4.99809L5.95343 4.99638C5.95343 4.8498 5.88082 4.71264 5.77798 4.60971L1.32006 0.159839C1.21713 0.0567483 1.07583 0.000163033 0.929321 -3.85729e-08C0.782734 -3.21654e-08 0.643383 0.0567484 0.540537 0.159839Z"
											fill="#28292B"></path>
							</svg>
					</a>
					<ul class="header-navbar__menutercernivel id-${categoria.Id}" style="display: none;">
					</ul>
				</li>
			`)

			categoria.Children.forEach(subcategoria => {
				console.log('subcategoria', subcategoria)
				$(`.header-navbar__menutercernivel.id-${categoria.Id}`).append(`
					<li class="header-navbar-submenu__item">
							<a href="${subcategoria.Link}">${subcategoria.Name}</a>
					</li>
				`)
			})
		});

	});
}