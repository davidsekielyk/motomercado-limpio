const vBuscadorPorMoto = {
  template: `
    <div class="v-buscador-por-moto">
      <div class="v-buscador-por-moto__fondo">
        <div class="v-buscador-por-moto__title">
          <h3>Encontrá todo lo que necesitas para tu moto</h3>
          <h5>Agregá todos los datos que conozcas para mejorar la busqueda</h5>
        </div>
        <div class="v-buscador-por-moto__seleccion">
          <div class="v-buscador-por-moto__campos">
            <div class="v-buscador-por-moto__campos--elegidos">
              <div class="v-buscador-por-moto__campo marca">
                <span class="v-buscador-por-moto__campo--sel">
                  Elegir >
                </span>
                <span class="v-buscador-por-moto__campo--title">
                  MARCA
                </span>
              </div>
              <div class="v-buscador-por-moto__campo marca">
                <span class="v-buscador-por-moto__campo--sel">
                  Elegir >
                </span>
                <span class="v-buscador-por-moto__campo--title">
                  MODELO
                </span>
              </div>
              <div class="v-buscador-por-moto__campo marca">
                <span class="v-buscador-por-moto__campo--sel">
                  Elegir >
                </span>
                <span class="v-buscador-por-moto__campo--title">
                  CILINDRADA
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="v-buscador-por-moto__img-moto">
        <div class="v-buscador-por-moto__btn-submit">
          <svg width="47" height="47" viewBox="0 0 47 47" fill="none" xmlns="http://www.w3.org/2000/svg">
            <circle cx="23.5" cy="23.5" r="23.5" fill="#FC5226"/>
            <path d="M14.6516 24.6216C16.0197 25.9952 17.6828 26.684 19.6412 26.688C21.5995 26.692 23.2655 26.0099 24.6391 24.6419C26.0127 23.2738 26.7015 21.6107 26.7055 19.6523C26.7095 17.694 26.0275 16.028 24.6594 14.6544C23.2914 13.2808 21.6282 12.592 19.6699 12.588C17.7115 12.584 16.0456 13.266 14.6719 14.6341C13.2983 16.0021 12.6095 17.6653 12.6055 19.6236C12.6016 21.582 13.2836 23.2479 14.6516 24.6216ZM29.0412 26.7071L36.8097 34.5073L34.4549 36.8525L26.6864 29.0523L26.6889 27.8039L26.2492 27.3624C24.3855 28.9742 22.1807 29.7775 19.6349 29.7723C16.7953 29.7666 14.3739 28.7825 12.3706 26.8201C10.4162 24.8577 9.44196 22.4568 9.44773 19.6172C9.45351 16.7776 10.4376 14.3807 12.3999 12.4263C14.4112 10.4231 16.8367 9.42442 19.6763 9.4302C22.5159 9.43597 24.9128 10.4445 26.867 12.4558C28.8214 14.4181 29.7957 16.819 29.7899 19.6586C29.7847 22.2044 28.9724 24.4059 27.353 26.263L27.7927 26.7046L29.0412 26.7071Z" fill="white"/>
          </svg>
        </div>
        <img src="/arquivos/ejemplo-moto.png" alt="moto ejemplo" />
      </div>
    </div>
  `
}

// if($('#buscador-por-moto').length){
//   new Vue({
//     el: '#buscador-por-moto',
//     components: {
//       'v-buscador-por-moto': vBuscadorPorMoto
//     }
//   })
// }
