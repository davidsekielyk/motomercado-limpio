const vFiltros = {
  template: `
    <aside class="v-filtros">
      <div class="v-filtros__list">
        
        <!-- MARCAS -->
        <div class="v-filtros__tipo marcas">
          <div class="v-filtros__tipo--list">
            <h3 class="v-filtros__title">
              Marcas
            </h3>
            <span v-for="(marca, index) in marcas" 
              :key="index" 
              :data-fetch="marca.dataFetch"
              :title="marca.title"
              @click="changeURL(marca.dataFetch)"
              class="v-filtro">
              <span class="v-filtros__text">
                {{ marca.name }}
              </span>
            </span>
          </div>

        </div>
      </div>
    </aside>
  `,
  data() {
    return {
      filtros: $(".search-single-navigator"),
    }
  },
  computed: {
    origin() {
      return window.location.origin
    },
    marcas() {
      const filtros = $(this.filtros).find('ul.Marca').find('a') 
      const marcas = Array.prototype.map.call(filtros, filtro => {
        return {
          name: $(filtro).html(),
          title: $(filtro).attr('title'),
          dataFetch: $(filtro).attr("href")
        }
      })
      return marcas
    },
  },
  methods: {
    changeURL(url) {
      url = url.replace(this.origin,"")
      router.push(url)
    },
    filtrar(url) {
      this.fetchPage(url)
      .then(page => {
        this.replaceContent(page)
      })
      .catch( e => {
        console.error(e)
      })
    },
    fetchPage(url) {
      return new Promise( ( res, rej ) => {
        $.ajax({
          url: url,
          success: page => {
            res(page)
          },
          error: e => {
            rej(e)
          }
        })
      })
    },
    replaceContent(page) {
      this.filtros = $(page).find(".search-single-navigator")
    }
  },
  watch: {
    $route( to, from ) {
      const url = to.query.lid === undefined ? to.fullPath+this.lid : to.fullPath
      this.filtrar(url)
    }
  }
};

const vProductos = {
  components: {
    'v-buscador-por-moto': vBuscadorPorMoto
  },
  template: `
    <article class="article">
      <v-buscador-por-moto />
      <div class="vitrines" v-html="products">
      </div>
    </article>
  `,
  data() {
    return {
      products: $('.vitrines .prateleira').html()
    }
  }
}

const Magia = {
  components: {
    'v-filtros': vFiltros,
    'v-productos': vProductos
  },
  template: `
    <div class="row">
      <v-filtros />
      <v-productos />
    </div>
  `
}